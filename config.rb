activate :autoprefixer do |prefix|
  prefix.browsers = "last 2 versions"
end

# Assets
set :css_dir, 'stylesheets'
set :js_dir, 'javascripts'
set :images_dir, 'images'
set :fonts_dir, 'fonts'

# Per-page layout changes
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/
helpers do
  def inline(name, elmt, dir)
    root = Middleman::Application.root
    file_path = app.root_path.join("source", dir, name)
    content = File.open(file_path).read if File.exists?(file_path) 
    return "<#{elmt}>#{content}</#{elmt}>" 
  end

  def inline_js(name)
    inline(name, 'script', config[:js_dir])
  end

  def inline_css(name)
    inline(name, 'style', config[:css_dir]) 
  end

#   # ref: https://www.seancdavis.com/blog/render-inline-svg-rails-middleman/
#   def svg(name)
#     root = Middleman::Application.root
#     file_path = "#{root}/source/images/#{name}.svg"
#     return File.read(file_path) if File.exists?(file_path)
#   end
end

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings

configure :build do
  ignore /stylesheets\/.*\.css/
  ignore /javascripts\/.*\.js/
  activate :minify_css
  activate :minify_javascript
  activate :relative_assets
end

configure :development do
  activate :livereload
end
