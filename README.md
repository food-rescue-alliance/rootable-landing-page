Plain [html page][htmlpage] for [Rootable][tomato], using GitLab Pages. 

---


## License

Rootable Landing Page © 2021 by [Food Rescue Alliance][fra] is licensed under [CC BY-NC 4.0 ][cc]

## Add your application to the alternatives section

We'd love to feature your application on our website. Create a pull request with the addition or contact us via the website contact form.

[cc]:https://creativecommons.org/licenses/by-nc/4.0
[fra]:https://foodrescuealliance.org
[htmlpage]:http://food-rescue-alliance.gitlab.io/rootable-landing-page
[tomato]:https://gitlab.com/food-rescue-alliance/tomato